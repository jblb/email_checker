#! /usr/bin/env python3
# -*- coding:utf8 -*-
#
import sys
import os.path

def check_email(email):

	import re
	import socket
	import smtplib
	import dns.resolver
	import dns.exception

	timeout_value = 60 # seconds
	socket.setdefaulttimeout(timeout_value)

	# Address used for SMTP MAIL FROM command
	fromAddress = 'email_checker@jblb.net'

	# Simple Regex for syntax checking
	regex = '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$'

	email=email.lower() # tout minuscule

	# Syntax check
	match = re.match(regex, email)
	if match == None:
		#print('Bad syntax')
		return False, 'Bad syntax'

	# Get domain for DNS lookup
	splitAddress = email.split('@')
	domain = str(splitAddress[1])
	#print('Domain:', domain)

	# MX record lookup
	try:
		records = dns.resolver.query(domain, 'MX')
		mxRecord = records[0].exchange
		mxRecord = str(mxRecord)
	except dns.resolver.NXDOMAIN:
		# print (f"Domain {domain} doesn't exist'")
		return False, 'No Domain'
	except dns.resolver.Timeout:
		# print (f"Domain {domain} timed out")
		return False, 'Time Out'
	except Exception as e:
		# print (f"Domain {domain} raise exception {e}")
		return False, f"Error {e} in MX lookup"


	# SMTP lib setup (use debug level for full output)
	server = smtplib.SMTP()
	server.set_debuglevel(0)

	# get local hostname from fromAddress
	local_Address = fromAddress.split('@')
	local_domain = str(local_Address[1])
	server.local_hostname = local_domain


	# SMTP Conversation
	try:
		server.connect(mxRecord)
		server.helo(server.local_hostname) ### server.local_hostname(Get local server hostname)
		server.mail(fromAddress)
		code, message = server.rcpt(str(email))
		server.quit()
	except Exception as e:
		return False, f"Error {e} in smtp"

	#print(code)
	#print(message)

	# Assume SMTP response 250 is success
	if code == 250:
		# print('Success')
		return True, None
	else:
		# print('Bad')
		return False,'No User'

#file process
def do_file_process(the_file):
	nb_mail = 1 # nombre de mail traités

	# get base file_name
	base_file_name = the_file.split('.')
	# print(f"base file name : {base_file_name[0]}")
	# create output file name
	good_file_name = base_file_name[0]+"_OK."+base_file_name[1]
	bad_file_name = base_file_name[0]+"_BAD."+base_file_name[1]

	# Open files
	source = open(the_file, "r")
	output_file_OK = open(good_file_name, "w")
	output_file_BAD =open (bad_file_name,"w")
	try:
		for the_mail in source:
		# supprimer le (ou les) caractère(s) de fin de ligne
			email = the_mail.rstrip('\n\r')
			email = email.lower() # tout minuscule
			if email : # si ligne pas vide
				print(f"{nb_mail}\t{email}", end=' ')
				test_mail = check_email(str(email))
				if test_mail[0] :
					nb_mail += 1
					output_file_OK.write("%s\n" % email)
					print (f"OK")
				else:
					output_file_BAD.write(f"{email}\t\t{test_mail[1]}\n")
					print(f"BAD")
			if nb_mail % 10 == 0 :
				output_file_OK.flush()
				output_file_BAD.flush()

	finally:
		source.close()
		output_file_OK.close()
		output_file_BAD.close()

	return


# Email address to verify
if sys.argv[1:]:
	in_file = str(sys.argv[1])
	if os.path.isfile(in_file):
		print(f"{in_file} exist, process it")
		do_file_process(in_file)
		quit()
	for email in sys.argv[1:]:
		test_mail = check_email(str(email))
		if test_mail[0] :
			print(f"{email} is valid")
		else:
			print(f"{email} is not valid, {test_mail[1]}")

else:
	inputAddress = input('Please enter the emailAddress to verify: ')
	email = str(inputAddress)
	test_mail = check_email(str(email))
	if test_mail[0] :
		print(f"{email} is valid ")
	else:
		print(f"{email} is not valid, {test_mail[1]}")
