# Python-Email-Verification-Script
A simple script for email address verification using syntax, DNS and mailbox verification

Based on: [https://www.scottbrady91.com/Email-Verification/Python-Email-Verification-Script](https://www.scottbrady91.com/Email-Verification/Python-Email-Verification-Script)

This script uses Python 3 and dnspython3

